﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnetcoreangular.Models
{
    public class Recipe
    {
        public int RecipeID { get; set; }

        [Required]
        public string RecipeName { get; set; }

        [Required]
        public string Details { get; set; }

        public byte[] Image { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }

        [NotMapped]
        [Display(Name = "Ingredients")]
        public string ingredientsString { get; set; }

        //public int UserID { get; set; }

        //navigation property
        //public virtual  User { get; set; }

        public virtual ICollection<Ingredient> Ingredients { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        /* [InverseProperty("Recipe")]
        public virtual ICollection<CartRecipe> CartRecipes {get; set;} */
    }
}
