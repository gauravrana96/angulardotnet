﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace dotnetcoreangular.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        //public DateTime DOB { get; set; }

        public virtual ICollection<Recipe> Recipes { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Cart> Carts { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            /*Database.SetInitializer(new IdentityDBInitializer());
            //Database.SetInitializer<DXContext>(null);// Remove default initializer
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false; */
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // This will singularize all table names
              foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes())
              {
                entityType.Relational().TableName = entityType.Name;
              }

            modelBuilder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    FirstName="Gaurav",
                    LastName="Rana",
                    Gender="Male",
                    Id="1",
                    Email="gaurav@gmail.com",
                    UserName="gaurav@gmail.com",
                    PasswordHash = "AQAAAAEAACcQAAAAEEmJ6DEA6m54DeFzo1rE4j0ikCzcbGyNHXyIQ27DQXDPulq+K/jo+ha0KikO1v1i0A=="
                }
                );
          modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id="1",
                    Name="Admin"
                }
                );
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId="1",
                    UserId="1"
                }
                );
                
        }

    }

   
}
