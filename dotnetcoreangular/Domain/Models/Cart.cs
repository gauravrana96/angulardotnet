﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnetcoreangular.Models
{
    public class Cart
    {
        public int CartID { get; set; }
        public int RecipeID { get; set; }
        public int Quantity { get; set; }

        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }

        //navigation property
        public virtual ApplicationUser ApplicationUser { get; set; }


        public virtual Recipe Recipe { get; set; }
    }
}
