﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnetcoreangular.Models
{
    public class Ingredient
    {
        public int IngredientID { get; set; }

        [Required]
        public string Name { get; set; }

        public byte[] Image { get; set; }

        [ForeignKey("Recipe")]
        public int RecipeID { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
