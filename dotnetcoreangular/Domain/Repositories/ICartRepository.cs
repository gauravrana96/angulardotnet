﻿using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<Cart> AddToCart(Cart cart);

        void DeleteFromCart(Cart cart);

        Task<Cart> UpdateQuantity(Cart cart);

        Task<IEnumerable<Cart>> GetAllAsync(string userId); 
    }
}
