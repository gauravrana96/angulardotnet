﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetcoreangular.Models;

namespace dotnetcoreangular.Domain.Repositories
{
    public interface IRecipeRepository
    {
        Task<IEnumerable<Recipe>> ListAsync();

        Task AddAsync(Recipe recipe);

        Task<Recipe> FindByIdAsync(int id);

        void Update(Recipe recipe);

        Task<Recipe> GetAsync(int id);

        void Delete(int id);
    }
}
