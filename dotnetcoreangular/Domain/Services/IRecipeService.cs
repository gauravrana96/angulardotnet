﻿using dotnetcoreangular.Domain.Services.Communication;
using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Domain.Services
{
    public interface IRecipeService
    {
        Task<IEnumerable<Recipe>> ListAsync();

        Task<SaveRecipeResponse> AddAsync(Recipe recipe);

        Task<SaveRecipeResponse> UpdateAsync(int id, Recipe recipe);

        Task<SaveRecipeResponse> GetAsync(int id);

        Task<SaveRecipeResponse> Delete(int id);
    }
}
