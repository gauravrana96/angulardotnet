﻿using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Domain.Services.Communication
{
    public class AddCartResponse: BaseResponse
    {
        public Cart Cart { get; private set; }

        private AddCartResponse(bool success, string message, Cart cart): base(success, message)
        {
            Cart = cart;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="cart">Saved category.</param>
        /// <returns>Response.</returns>
        public AddCartResponse(Cart cart) : this(true, string.Empty, cart)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public AddCartResponse(string message) : this(false, message, null)
        { }
    }
}
