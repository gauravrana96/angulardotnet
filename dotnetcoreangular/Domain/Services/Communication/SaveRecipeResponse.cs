﻿using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Domain.Services.Communication
{
    public class SaveRecipeResponse: BaseResponse
    {
        public Recipe Recipe { get; private set; }

        private SaveRecipeResponse(bool success, string message, Recipe recipe) : base(success, message)
        {
            Recipe = recipe;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="recipe">Saved category.</param>
        /// <returns>Response.</returns>
        public SaveRecipeResponse(Recipe recipe) : this(true, string.Empty, recipe)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public SaveRecipeResponse(string message) : this(false, message, null)
        { }
    }
}
