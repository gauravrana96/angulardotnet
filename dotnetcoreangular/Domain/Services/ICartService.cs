﻿using dotnetcoreangular.Domain.Services.Communication;
using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Domain.Services
{
    public interface ICartService
    {
        Task<AddCartResponse> AddToCart(Cart cart);

        Task<AddCartResponse> DeleteFromCart(Cart cart);

        Task<AddCartResponse> UpdateQuantity(Cart cart);

        Task<IEnumerable<Cart>> GetAllAsync(string userId);
    }
}
