﻿using dotnetcoreangular.Domain.Repositories;
using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace dotnetcoreangular.Persistence.Repositories
{
    public class CartRepository : BaseRepository, ICartRepository
    {
        public CartRepository(ApplicationDbContext context) : base(context)
        {
        }


        public async Task<Cart> AddToCart(Cart cart)
        {
            await _context.Carts.AddAsync(cart);
            return cart;
        }

        public void DeleteFromCart(Cart cart)
        {
             _context.Carts.Remove(cart);
        }

        public async Task<Cart> UpdateQuantity(Cart cart)
        {
            var cartFound = await _context.Carts.FindAsync(cart.CartID);
            cartFound.Quantity = cart.Quantity;

            _context.Carts.Update(cartFound);
            return cartFound;
        }

        public async Task<IEnumerable<Cart>> GetAllAsync(string userId)
        {
            return await _context.Carts.Where(x => x.ApplicationUserID == userId).Include(cart => cart.Recipe).ToListAsync();
        }
    }
}
