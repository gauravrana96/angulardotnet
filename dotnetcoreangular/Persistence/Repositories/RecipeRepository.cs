﻿using dotnetcoreangular.Models;
using dotnetcoreangular.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Persistence.Repositories
{
    public class RecipeRepository : BaseRepository, IRecipeRepository
    {
        public RecipeRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Recipe>> ListAsync()
        {
            //return await _context.Recipes.Include(recipe => recipe.Ingredients).ToListAsync();
            return await _context.Recipes.Include(recipe => recipe.Ingredients).ToListAsync();
        }

        public async Task AddAsync(Recipe recipe)
        {
            await _context.Recipes.AddAsync(recipe);

            await _context.SaveChangesAsync();


        }

        public async Task<Recipe> FindByIdAsync(int id)
        {
            return await _context.Recipes.FindAsync(id);
        }

        public void Update(Recipe recipe)
        {
            
            var oldRecipe =  _context.Recipes.Find(recipe.RecipeID);
            oldRecipe.RecipeName = recipe.RecipeName;
            oldRecipe.Image = recipe.Image;
            oldRecipe.Details = recipe.Details;
            oldRecipe.Price = recipe.Price;

            var oldIngs = _context.Ingredients.Where(ing => ing.RecipeID == oldRecipe.RecipeID);
            _context.RemoveRange(oldIngs);
            oldRecipe.Ingredients = recipe.Ingredients;

            _context.Recipes.Update(oldRecipe);
            //_context.Entry(oldRecipe).State = EntityState.Modified;
        }

        public async Task<Recipe> GetAsync(int id)
        {
            return await _context.Recipes.FindAsync(id);
        }

        public void Delete(int id)
        {
            var recipe = new Recipe { RecipeID = id };
            _context.Recipes.Remove(recipe);
        }
    }
}
