﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetcoreangular.Models;

namespace dotnetcoreangular.Resources
{
    public class RecipeResource
    {
        public int RecipeID { get; set; }
        
        public string RecipeName { get; set; }

        public string Details { get; set; }

        public byte[] Image { get; set; }
        
        public decimal Price { get; set; }

        public string ApplicationUserID { get; set; }

        public List<IngredientResource> Ingredients { get; set; }
    }
}
