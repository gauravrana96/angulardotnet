﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using dotnetcoreangular.Models;

namespace dotnetcoreangular.Resources
{
    public class SaveRecipeResource
    {
        [Required]
        public string RecipeName { get; set; }

        [Required]
        public string Details { get; set; }

        public byte[] Image { get; set; }

        [Required]
        public decimal Price { get; set; }

        public List<Ingredient> Ingredients { get; set; }
    }
}
