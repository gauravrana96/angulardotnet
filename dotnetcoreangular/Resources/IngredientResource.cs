﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Resources
{
    public class IngredientResource
    {
        public int IngredientID { get; set; }
        
        public string Name { get; set; }

        public byte[] Image { get; set; }
        
        public int RecipeID { get; set; }
    }
}
