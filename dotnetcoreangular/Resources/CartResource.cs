﻿using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Resources
{
    public class CartResource
    {
        public int CartID { get; set; }

        [Required]
        public int RecipeID { get; set; }

        [Required]
        public int Quantity { get; set; }

        public string ApplicationUserID { get; set; }

        public RecipeResource Recipe { get; set; }
    }
}
