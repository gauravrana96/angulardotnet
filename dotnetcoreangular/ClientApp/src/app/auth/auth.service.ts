import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

@Injectable()
export class AuthService {

    baseUrl = "https://localhost:44301/api";

    loginUrl = this.baseUrl + '/account/login';
    registerUrl = this.baseUrl + '/account/register';

    constructor(private http: HttpClient) {}

    login(user) {
        return this.http.post(this.loginUrl, user, httpOptions);
    }

    register(user) {
        return this.http.post(this.registerUrl, user, httpOptions);
    }

    saveToken(token) {
        localStorage.setItem('currentUser', JSON.stringify(token));
    }

    fetchToken() {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token;
    }

    removeToken() {
        localStorage.removeItem('currentUser');
    }
}