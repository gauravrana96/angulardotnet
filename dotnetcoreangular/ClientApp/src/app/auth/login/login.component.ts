import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private error = null;

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]]
  });

  constructor(
      private fb: FormBuilder, 
      private authService: AuthService,
      private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.loginForm.value)
      .subscribe(
        data => {
          this.authService.saveToken(data);
          this.router.navigate(["/recipes"]);
        },
        err => {
          this.error = err;
          console.log(err);
        }
    );
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

}
