import { ValidatorFn, FormGroup, ValidationErrors } from "@angular/forms";

// for matching pwd and confirm pwd

export const confirmPasswordValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');

    return password && confirmPassword && password.value === confirmPassword.value ? null : {matchingInputs: false};
}