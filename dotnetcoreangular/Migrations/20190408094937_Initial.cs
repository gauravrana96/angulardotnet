﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dotnetcoreangular.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dotnetcoreangular.Models.ApplicationUser",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dotnetcoreangular.Models.ApplicationUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityRole",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityRole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dotnetcoreangular.Models.Recipe",
                columns: table => new
                {
                    RecipeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RecipeName = table.Column<string>(nullable: false),
                    Details = table.Column<string>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    ApplicationUserID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dotnetcoreangular.Models.Recipe", x => x.RecipeID);
                    table.ForeignKey(
                        name: "FK_dotnetcoreangular.Models.Recipe_dotnetcoreangular.Models.ApplicationUser_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserClaim<string>",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityUserClaim<string>_dotnetcoreangular.Models.ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserLogin<string>",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityUserLogin<string>_dotnetcoreangular.Models.ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserToken<string>",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityUserToken<string>", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityUserToken<string>_dotnetcoreangular.Models.ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>_Microsoft.AspNetCore.Identity.IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Microsoft.AspNetCore.Identity.IdentityRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserRole<string>",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Microsoft.AspNetCore.Identity.IdentityUserRole<string>", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityUserRole<string>_Microsoft.AspNetCore.Identity.IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Microsoft.AspNetCore.Identity.IdentityRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Microsoft.AspNetCore.Identity.IdentityUserRole<string>_dotnetcoreangular.Models.ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dotnetcoreangular.Models.Cart",
                columns: table => new
                {
                    CartID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RecipeID = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    ApplicationUserID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dotnetcoreangular.Models.Cart", x => x.CartID);
                    table.ForeignKey(
                        name: "FK_dotnetcoreangular.Models.Cart_dotnetcoreangular.Models.ApplicationUser_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "dotnetcoreangular.Models.ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dotnetcoreangular.Models.Cart_dotnetcoreangular.Models.Recipe_RecipeID",
                        column: x => x.RecipeID,
                        principalTable: "dotnetcoreangular.Models.Recipe",
                        principalColumn: "RecipeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dotnetcoreangular.Models.Ingredient",
                columns: table => new
                {
                    IngredientID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    RecipeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dotnetcoreangular.Models.Ingredient", x => x.IngredientID);
                    table.ForeignKey(
                        name: "FK_dotnetcoreangular.Models.Ingredient_dotnetcoreangular.Models.Recipe_RecipeID",
                        column: x => x.RecipeID,
                        principalTable: "dotnetcoreangular.Models.Recipe",
                        principalColumn: "RecipeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Microsoft.AspNetCore.Identity.IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1", "811a63d8-cdf0-463f-8e1a-1dc9a0197849", "Admin", null });

            migrationBuilder.InsertData(
                table: "dotnetcoreangular.Models.ApplicationUser",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "Gender", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "1", 0, "4d344822-351a-48f1-a823-e593e0072290", "gaurav@gmail.com", false, "Gaurav", "Male", "Rana", false, null, null, null, "AQAAAAEAACcQAAAAEEmJ6DEA6m54DeFzo1rE4j0ikCzcbGyNHXyIQ27DQXDPulq+K/jo+ha0KikO1v1i0A==", null, false, null, false, "gaurav@gmail.com" });

            migrationBuilder.InsertData(
                table: "Microsoft.AspNetCore.Identity.IdentityUserRole<string>",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "1", "1" });

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "dotnetcoreangular.Models.ApplicationUser",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "dotnetcoreangular.Models.ApplicationUser",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_dotnetcoreangular.Models.Cart_ApplicationUserID",
                table: "dotnetcoreangular.Models.Cart",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_dotnetcoreangular.Models.Cart_RecipeID",
                table: "dotnetcoreangular.Models.Cart",
                column: "RecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_dotnetcoreangular.Models.Ingredient_RecipeID",
                table: "dotnetcoreangular.Models.Ingredient",
                column: "RecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_dotnetcoreangular.Models.Recipe_ApplicationUserID",
                table: "dotnetcoreangular.Models.Recipe",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Microsoft.AspNetCore.Identity.IdentityRole",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>_RoleId",
                table: "Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Microsoft.AspNetCore.Identity.IdentityUserClaim<string>_UserId",
                table: "Microsoft.AspNetCore.Identity.IdentityUserClaim<string>",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Microsoft.AspNetCore.Identity.IdentityUserLogin<string>_UserId",
                table: "Microsoft.AspNetCore.Identity.IdentityUserLogin<string>",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Microsoft.AspNetCore.Identity.IdentityUserRole<string>_RoleId",
                table: "Microsoft.AspNetCore.Identity.IdentityUserRole<string>",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dotnetcoreangular.Models.Cart");

            migrationBuilder.DropTable(
                name: "dotnetcoreangular.Models.Ingredient");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserClaim<string>");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserLogin<string>");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserRole<string>");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityUserToken<string>");

            migrationBuilder.DropTable(
                name: "dotnetcoreangular.Models.Recipe");

            migrationBuilder.DropTable(
                name: "Microsoft.AspNetCore.Identity.IdentityRole");

            migrationBuilder.DropTable(
                name: "dotnetcoreangular.Models.ApplicationUser");
        }
    }
}
