﻿using AutoMapper;
using dotnetcoreangular.Models;
using dotnetcoreangular.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Mapping
{
    public class ModelToResourceProfile: Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Recipe, RecipeResource>();
            CreateMap<Ingredient, IngredientResource>();
            CreateMap<Cart, CartResource>();
        }
    }
}
