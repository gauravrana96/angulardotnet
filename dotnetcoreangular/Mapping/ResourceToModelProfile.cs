﻿using AutoMapper;
using dotnetcoreangular.Models;
using dotnetcoreangular.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Mapping
{
    public class ResourceToModelProfile: Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SaveRecipeResource, Recipe>();
            CreateMap<CartResource, Cart>();
        }
    }
}
