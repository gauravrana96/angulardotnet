﻿using dotnetcoreangular.Models;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using dotnetcoreangular.Domain.Services;
using AutoMapper;
using dotnetcoreangular.Resources;
using dotnetcoreangular.Extensions;
using System.Linq;

namespace dotnetcoreangular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class RecipeController : ControllerBase
    {
        private readonly IRecipeService _recipeService;
        private readonly IMapper _mapper;


        public RecipeController(IRecipeService recipeService, IMapper mapper)
        {
            _recipeService = recipeService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<RecipeResource>> GetAllAsync()
        {        
            //var userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var recipes = await _recipeService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Recipe>, IEnumerable<RecipeResource>>(recipes);
            return resources;
        }



        [HttpPost]
        public async Task<IActionResult> AddRecipe([FromBody] SaveRecipeResource resource)
        {
            var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value;

            if(role == "Admin")
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var recipe = _mapper.Map<SaveRecipeResource, Recipe>(resource);
                recipe.ApplicationUserID = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                var result = await _recipeService.AddAsync(recipe);

                if (!result.Success)
                    return BadRequest(result.Message);

                var recipeResource = _mapper.Map<Recipe, RecipeResource>(result.Recipe);
                return Ok(recipeResource);
            }
            return BadRequest("Not Authorised");           

           
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateRecipe(int id, [FromBody] SaveRecipeResource resource)
        {
            var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value;
            if(role == "Admin")
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var recipe = _mapper.Map<SaveRecipeResource, Recipe>(resource);

                recipe.ApplicationUserID = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                recipe.RecipeID = id;


                var result = await _recipeService.UpdateAsync(id, recipe);

                if (!result.Success)
                    return BadRequest(result.Message);

                var recipeResource = _mapper.Map<Recipe, RecipeResource>(result.Recipe);
                recipe.RecipeID = id;
                return Ok(recipeResource);
            }

            return BadRequest("Not Authorised");
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetRecipe(int id)
        {
            var result = await _recipeService.GetAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var recipeResource = _mapper.Map<Recipe, RecipeResource>(result.Recipe);
            return Ok(recipeResource);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteRecipe(int id)
        {
            var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value;
            if (role == "Admin")
            {
                var result = await _recipeService.Delete(id);

                if (!result.Success)
                    return BadRequest(result.Message);

                return Ok();
            }

            return BadRequest("Not Authorised");
            
        }
    }
}