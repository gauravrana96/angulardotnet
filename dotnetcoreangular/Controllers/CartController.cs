﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using dotnetcoreangular.Domain.Services;
using dotnetcoreangular.Models;
using dotnetcoreangular.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using dotnetcoreangular.Extensions;

namespace dotnetcoreangular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;


        public CartController(ICartService cartService, IMapper mapper)
        {
            _cartService = cartService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<CartResource>> GetAll()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var cartItems = await _cartService.GetAllAsync(userId);

            var cartResources = _mapper.Map<IEnumerable<Cart>, IEnumerable<CartResource>>(cartItems);
            return cartResources;
            
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] CartResource cartResource)
        {
            var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value;
            if(role != "Admin")
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var cart = _mapper.Map<CartResource, Cart>(cartResource);
                cart.ApplicationUserID = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var result = await _cartService.AddToCart(cart);

                if (!result.Success)
                    return BadRequest(result.Message);

                var recipeResource = _mapper.Map<Cart, CartResource>(result.Cart);
                return Ok(recipeResource);
            }

            return BadRequest("This operation not allowed for Admin");

        }

        [HttpDelete]
        public async Task<IActionResult> Delete(CartResource cartResource)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (cartResource.ApplicationUserID == userId)
            {
                var cart = _mapper.Map<CartResource, Cart>(cartResource);
                var result = await _cartService.DeleteFromCart(cart);

                if (!result.Success)
                    return BadRequest(result.Message);

                return Ok();
            }

            return BadRequest("NOT Authorised");

        }

        [HttpPut]
        public async Task<IActionResult> Update(CartResource cartResource)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (cartResource.ApplicationUserID == userId)
            {
                var cart = _mapper.Map<CartResource, Cart>(cartResource);
                var result = await _cartService.UpdateQuantity(cart);

                if (!result.Success)
                    return BadRequest(result.Message);

                return Ok();
            }

            return BadRequest("NOT Authorised");

        }

    }
}