﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using dotnetcoreangular.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace dotnetcoreangular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private IPasswordHasher<ApplicationUser> _passwordHasher;

        public AccountController(
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext db,
            IConfiguration configuration,
            ILogger<AccountController> logger,
            IPasswordHasher<ApplicationUser> passwordHasher)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _db = db;
            _configuration = configuration;
            _logger = logger;
            _passwordHasher = passwordHasher;
        }

        [HttpPost("[action]")]
        public async Task<object> Register(User userPost)
        {
            Debug.WriteLine("User Name: " + userPost.FirstName);
            var user = new ApplicationUser
            {
                UserName = userPost.Email,
                Email = userPost.Email,
                FirstName = userPost.FirstName,
                LastName = userPost.LastName,
                Gender=userPost.Gender
            };

            var result = await _userManager.CreateAsync(user, userPost.Password);
            Debug.Write("Line: " + result);
            if (result.Succeeded)
            {
                if(! await _roleManager.RoleExistsAsync("User"))
                {
                    var role = new IdentityRole();
                    role.Name = "User";
                    await _roleManager.CreateAsync(role);
                }
                await _userManager.AddToRoleAsync(user, "User");
                _logger.LogInformation("User created a new account with password.");
                
                var tokenString = await GenerateJwtToken(user);
                return Ok(new { token = tokenString });

            }
            return BadRequest();
        }

        [HttpPost("[action]")]
        public async Task<object> Login(User userPost)
        {
            //var result = await _signInManager.PasswordSignInAsync(userPost.Email , userPost.Password, false, lockoutOnFailure: false);

            var user = _db.Users.Where(u => u.Email == userPost.Email).FirstOrDefault();
            
            if (user != null)
            {
                PasswordVerificationResult result = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, userPost.Password);
                if (result.ToString()=="Success")
                {
                    var appUser = _userManager.Users.SingleOrDefault(u => u.Email == userPost.Email);
                    _logger.LogInformation("User logged in.");
                    var tokenString = await GenerateJwtToken(appUser);
                    return Ok(new { token = tokenString });
                }
            }           

            throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> Signout()
        {
            if (User.Identity.IsAuthenticated) {
                await _signInManager.SignOutAsync();
                _logger.LogInformation("User logged out.");
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("[action]")]
        public ActionResult CheckUser()
        {
            var isAuth = User.Identity.IsAuthenticated;

            return Ok(isAuth);
        }


        //GENERATE Token
        private async Task<object> GenerateJwtToken(ApplicationUser user)
        {
            string role;
            var roles = await _userManager.GetRolesAsync(user);
            role = roles.FirstOrDefault();

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim("role", role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}