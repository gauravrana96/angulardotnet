﻿using dotnetcoreangular.Domain.Repositories;
using dotnetcoreangular.Domain.Services;
using dotnetcoreangular.Domain.Services.Communication;
using dotnetcoreangular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetcoreangular.Services
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CartService(ICartRepository cartRepository, IUnitOfWork unitOfWork)
        {
            this._cartRepository = cartRepository;
            this._unitOfWork = unitOfWork;
        }

        public async Task<AddCartResponse> AddToCart(Cart cart)
        {
            try
            {
                await _cartRepository.AddToCart(cart);
                await _unitOfWork.CompleteAsync();

                return new AddCartResponse(cart);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new AddCartResponse($"An error occurred when adding to Cart: {ex.Message}");
            }

        }

        public async Task<AddCartResponse> DeleteFromCart(Cart cart)
        {
            try
            {
                 _cartRepository.DeleteFromCart(cart);
                await _unitOfWork.CompleteAsync();

                return new AddCartResponse(new Cart());
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new AddCartResponse($"An error occurred when deleting from Cart: {ex.Message}");
            }
        }

        public async Task<IEnumerable<Cart>> GetAllAsync(string userId)
        {
                return await _cartRepository.GetAllAsync(userId);               
           
        }

        public async Task<AddCartResponse> UpdateQuantity(Cart cart)
        {
            try
            {
                await _cartRepository.UpdateQuantity(cart);
                await _unitOfWork.CompleteAsync();

                return new AddCartResponse(cart);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new AddCartResponse($"An error occurred when updating the Quantity: {ex.Message}");
            }
        }
    }
}
