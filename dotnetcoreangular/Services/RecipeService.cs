﻿using dotnetcoreangular.Models;
using dotnetcoreangular.Domain.Repositories;
using dotnetcoreangular.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetcoreangular.Domain.Services.Communication;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;


namespace dotnetcoreangular.Services
{
    public class RecipeService: IRecipeService
    {

        private readonly IRecipeRepository _recipeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RecipeService(IRecipeRepository recipeRepository, IUnitOfWork unitOfWork)
        {
            this._recipeRepository = recipeRepository;
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Recipe>> ListAsync()
        {
            return await _recipeRepository.ListAsync();
        }


        public async Task<SaveRecipeResponse> AddAsync(Recipe recipe)
        {
            try
            {
                await _recipeRepository.AddAsync(recipe);
                await _unitOfWork.CompleteAsync();

                return new SaveRecipeResponse(recipe);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new SaveRecipeResponse($"An error occurred when saving the category: {ex.Message}");
            }
        }

        public async Task<SaveRecipeResponse> UpdateAsync(int id, Recipe recipe)
        {
            var existingRecipe = await _recipeRepository.FindByIdAsync(id);

            if (existingRecipe == null)
                return new SaveRecipeResponse("Recipe not found.");
            //existingRecipe.ApplicationUserID = recipe.ApplicationUserID;
            existingRecipe.RecipeName = recipe.RecipeName;
            existingRecipe.Details = recipe.Details;
            existingRecipe.Price = recipe.Price;
            existingRecipe.Image = recipe.Image;

            try
            {
                 _recipeRepository.Update(recipe);
                await _unitOfWork.CompleteAsync();

                return new SaveRecipeResponse(existingRecipe);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new SaveRecipeResponse($"An error occurred when updating the recipe: {ex.Message}");
            }
        }

        public async Task<SaveRecipeResponse> GetAsync(int id)
        {
            try
            {
                var recipe = await _recipeRepository.GetAsync(id);
                if (recipe == null)
                    return new SaveRecipeResponse("Recipe not Found.");
                return new SaveRecipeResponse(recipe);
            }
            catch(Exception ex)
            {
                return new SaveRecipeResponse($"An error occurred when retreiving the recipe: {ex.Message}");
            }
           
        }

        public async Task<SaveRecipeResponse> Delete(int id)
        {
            try
            {
                _recipeRepository.Delete(id);
                await _unitOfWork.CompleteAsync();

                return new SaveRecipeResponse(new Recipe());
            }
            catch(Exception ex)
            {
                return new SaveRecipeResponse($"An error occurred when deleting the recipe: {ex.Message}");
            }
            
        }
    }
}
